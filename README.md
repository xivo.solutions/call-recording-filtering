## Call Recording Filtering

:warning: This project is now **DEPRECATED**

See project [xivocc-recording](xivo.solutions/xivocc-recording) and documentation [about
recording](https://documentation.xivo.solutions/en/latest/xivo/upgrade/xivocc_recording.html#upgrade-recording-xpbx)
